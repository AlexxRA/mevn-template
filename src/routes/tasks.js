const express = require('express');
const router = express.Router();

const Task = require('../models/Task');
const { request } = require('express');

//Get all tasks
router.get('/', async (request, response) => {
  const tasks = await Task.find();
  response.json(tasks);
});

//Get one task
router.get('/:id', async (request, response) => {
  const task = await Task.findById(request.params.id);
  response.json(task);
});

//Save one task
router.post('/', async (request, response) => {
  const task = new Task(request.body);
  await task.save();
  response.json({
    status: 'Task saved'
  });
});

//Edit one task
router.put('/:id', async (request, response) => {
  await Task.findByIdAndUpdate(request.params.id, request.body);
  response.json({
    status: 'Task updated'
  });
});

//Delete one task
router.delete('/:id', async (request, response) => {
  await Task.findByIdAndRemove(request.params.id);
  response.json({
    status: 'Task deleted'
  });
});

module.exports = router;