import Vue from 'vue';
import App from './App.vue';
import router from './router'
import Vuetify from 'vuetify'

// import CreateItem from './components/CreateItem.vue';
// import DisplayItem from './components/DisplayItem.vue';
// import EditItem from './components/EditItem.vue';

const routes = [
  // {
  //   name: 'DisplayItem',
  //   path: '/',
  //   component: DisplayItem
  // },
  // {
  //   name: 'CreateItem',
  //   path: '/create/item',
  //   component: CreateItem
  // },
  // {
  //   name: 'EditItem',
  //   path: '/edit/:id',
  //   component: EditItem
  // }
];
new Vue({
  Vuetify,
  router,
  render: h => h(App)
}).$mount('#app')